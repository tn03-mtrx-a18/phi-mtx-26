//Image.c
#include<stdio.h> 
#include<stdlib.h>
#include"Image.h"

//function prototypes
Image readImage(FILE *fp,HEADER header, DIBHEADER dibheader,Image img;);
int writeImage(FILE *fp,FILE *fpw,HEADER header,DIBHEADER dibheader,Image img);

void main(){
	FILE *fp=fopen("image.bmp","rb");         //reading a bmp file  
	
    //variable declaration
    HEADER header;   
	DIBHEADER dibheader;
	Image img;	
	
    fread(header.name,2,1,fp);
	fread(&header.size,3*sizeof(int),1,fp);
	fread(&dibheader,sizeof(DIBHEADER),1,fp);
    fseek(fp,header.imageoffset,SEEK_SET); 
	Image image=readImage(fp,header,dibheader,image); //calling function
	FILE *fpw =fopen("new.bmp","w");                 //writing a bmp file
	writeImage(fp,fpw,header, dibheader,image);		 //calling function	
}


