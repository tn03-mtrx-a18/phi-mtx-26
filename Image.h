//Image.h
typedef struct BITMAPHEADER{	
	char name[2];   //Bitmapfileheader: First 2 bytes. BM if bmp file	
	unsigned int size;  //bmp file Size
	int g;  //garbage value 
	unsigned int imageoffset;  	//location from where image info starts in the file
}HEADER; 

typedef struct DIBheader{
	unsigned int headersize; //size of header
	unsigned int width;        //pixelwidth
	unsigned int height;      //pixelheight
	unsigned int temp[7];     //reserved for other info
}DIBHEADER;

struct RGB{
	unsigned char blue;
	unsigned char green;
	unsigned char red;
};

typedef struct IMAGE{
	int height;
	int width;
	struct RGB **rgb;  
}Image;	

//READING BMP FILE

Image readImage(FILE *fp,HEADER header, DIBHEADER dibheader ,Image img){ 	

int height =dibheader.height;
int width =dibheader.width;

     int i; 	
     
	img.rgb=(struct RGB**)malloc(height*sizeof(void*));
	img.height=height;
 	img.width= width;
    
    	for(i=height-1;i>=0;i--){
 		img.rgb[i]=(struct RGB*)malloc(width*sizeof(struct RGB));
 		fread(img.rgb[i],width,sizeof(struct RGB),fp);
 			 }
	 return img;
}

//WRITING BMP FILE

int writeImage(FILE *fp, FILE *fpw, HEADER header,DIBHEADER dibheader,Image img){

int height =dibheader.height;
int width =dibheader.width;

int i; 
	
    fwrite(header.name,2,1,fpw);
	fwrite(&header.size,3*sizeof(int),1,fpw);
	fwrite(&dibheader,sizeof(DIBHEADER),1,fpw);

for(i=img.height-1;i>=0;i--){
 		fwrite(img.rgb[i],img.width,sizeof(struct RGB),fpw);
	 }
fclose(fpw);
return 0;	
}


